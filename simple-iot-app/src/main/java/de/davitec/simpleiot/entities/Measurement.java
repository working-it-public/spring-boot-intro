package de.davitec.simpleiot.entities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * This is the JPA stored pendant for a measurement
 */
@Entity
@Table(name="measurements")
public class Measurement {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	long id;

	private Timestamp measured_at;
	private String identifier;
	private int value;
    private String unit;


	public Measurement() {	}

//    public Measurement(String identifier, int value, String unit) {
//        this.measured_at = new Timestamp(System.currentTimeMillis());
//        this.identifier = identifier;
//        this.value = value;
//        this.unit = unit;
//    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getMeasured_at() {
        return measured_at;
    }
    public void setMeasured_at(Timestamp measured_at) {
        this.measured_at = measured_at;
    }

    public String getIdentifier() {
        return identifier;
    }
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getValue() { return value; }
    public void setValue(int value) { this.value = value; }

    public String getUnit() {
        return unit;
    }
    public void setUnit(String unit) {
        this.unit = unit;
    }
}
