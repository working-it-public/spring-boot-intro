package de.davitec.simpleiot.repositories;

import de.davitec.simpleiot.entities.Measurement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface MeasurementRepository extends JpaRepository<Measurement, Long> {

}
