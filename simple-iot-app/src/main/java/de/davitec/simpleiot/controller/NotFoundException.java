package de.davitec.simpleiot.controller;

class NotFoundException extends RuntimeException {
    NotFoundException(String name, Long id) {
        super("Could not find " + name + " with Id=" + id);
    }
}
