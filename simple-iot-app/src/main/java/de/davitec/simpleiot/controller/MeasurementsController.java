package de.davitec.simpleiot.controller;

import de.davitec.simpleiot.entities.Measurement;
import de.davitec.simpleiot.repositories.MeasurementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
@RequestMapping("/api")
public class MeasurementsController {
    private static final Logger log = LoggerFactory.getLogger(MeasurementsController.class);

    @Autowired
    MeasurementRepository repository;

    @GetMapping("/measurements")
    List<Measurement> index() {
        List<Measurement> list = repository.findAll();
        log.debug("List "+list.size()+" measurements");
        return list;
    }

    @GetMapping("/measurements/{id}")
    Measurement show(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(
                ()-> new NotFoundException("Measurement", id)
        );
    }

    @PostMapping("/measurements")
    Measurement create(@RequestBody Measurement newMeasurement) {
        newMeasurement.setMeasured_at(new Timestamp(System.currentTimeMillis()));
        return repository.save(newMeasurement);
    }
}
