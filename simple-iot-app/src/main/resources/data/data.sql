insert into measurements (measured_at, identifier, value, unit)
  values ('2020-10-09T13:23:04Z', 'LU-Temp', 23, '°C'),
         ('2020-10-09T13:24:14Z', 'LU-Temp', 25, '°C'),
         ('2020-10-09T13:26:52Z', 'LU-Temp', 26, '°C')
;
