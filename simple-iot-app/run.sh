#!/bin/sh
if [ "$1" != "bash" ]; then
  exec java ${JAVA_OPTS} -jar app.jar ${@}
else
  $*
fi
